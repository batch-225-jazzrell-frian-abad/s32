// Node.js Routing with HTTP methods 
let http = require('http');

const server = http.createServer(function (request, response) {

	// The HTTP method of the incoming request can be accessed via the "method" property of the "request" parameter.
	// the method "GET" means that we will be retrieving or reading information or data 
 	if(request.url == '/items' && request.method == 'GET') {

 		response.writeHead(200, {'Content-Type' : 'text/plain'});

 		response.end('Data retrieve from the database');
	}

	// The method 'POST' means that we will be adding or creating information but for now, we will just be sending a test response for now.
	else if(request.url == '/items' && request.method == 'POST') {

 		response.writeHead(200, {'Content-Type' : 'text/plain'});

 		response.end('Data to be sent to the database');
	}


}).listen(4000);

console.log('Server is running at localhost:4000');