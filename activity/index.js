let http = require('http');

const server = http.createServer( (request, response) => {


	if (request.url == '/profile') {
		response.writeHead(200, { 'Content-Type': 'text/plain' });
		response.end("Welcome to your profile!");
	} else if (request.url == '/courses'){
		response.writeHead(200, { 'Content-Type' : 'text/plain' });
		response.end("Here’s our courses available");
	} else if (request.url == '/courses' && request.method == 'GET'){
		response.writeHead(404, {'Content-Type' : 'text/plain'});
		response.end("Add course to our resouces");
	} else if (request.url == '/addcourse' && request.method == 'POST'){
		response.writeHead(404, {'Content-Type' : 'text/plain'});
		response.end("Add course to our resources");
	} else if (request.url == '/updatecourse' && request.method == 'PUT'){
		response.writeHead(404, {'Content-Type' : 'text/plain'});
		response.end("Update a course to our resources");
	} else if (request.url == '/archivecourses' && request.method == 'DELETE'){
		response.writeHead(404, {'Content-Type' : 'text/plain'});
		response.end("Archive courses to our resources");
	} else {
		response.writeHead(404, {'Content-Type' : 'text/plain'});
		response.end("Welcome to Booking System");
	} 

}).listen(4000);

console.log("Server is running at localhost:4000");

